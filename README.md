Role Name
=========

Update Yum/Apt packages to latest versions and reboot if required



Example Playbook
----------------

````
---
- hosts: all
  become: true
  gather_facts: true
  vars:
    allow_reboot_for_updates: yes
  roles:
    - ansible-sysupdates

````

